# Milk Calculator - Angular Demo #

Prototype of app concept for calculating how much milk to add to a bowl cereal for the perfect ratio of milk to cereal. 

- User logs in to a shared account
- User selects individual profile
- User pours cereal into bowl
- Client-side app simulates weighing of poured cereal
- Simulated server-side app calculates milk recommendation
- Client-side app displays recommendation