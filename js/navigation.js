(function () {
  
  var nav = angular.module('navigation', []);

  nav.controller('NavigationController',['$scope', function ($scope) {
    var views = ['login', 'userList', 'pourCereal', 'weighCereal', 'recommendation'];
    var index = 0;
    $scope.view = views[index];
    $scope.nextView = function () {
      if (index === views.length - 1) {
        index = 0;
      } else {
        index = index + 1;
      }
      $scope.view = views[index];
    };
  }]);

  nav.directive('footer', function () {
    return {
      restrict: 'E',
      templateUrl: 'directives/footer.html'
    };
  });

  nav.directive('login', function () {
    return {
      restrict: 'E',
      templateUrl: 'directives/login.html'
    };
  });

  nav.directive('pourCereal', function () {
    return {
      restrict: 'E',
      templateUrl: 'directives/pourCereal.html'
    };
  });
  
  nav.directive('recommendation', function () {
    return {
      restrict: 'E',
      templateUrl: 'directives/recommendation.html'
    };
  });

  nav.directive('userProfile', function () {
    return {
      restrict: 'E',
      templateUrl: 'directives/userProfile.html'
    };
  });

  nav.directive('userList', function () {
    return {
      restrict: 'E',
      templateUrl: 'directives/userList.html'
    };
  });

  nav.directive('weighCereal', function () {
    return {
      restrict: 'E',
      templateUrl: 'directives/weighCereal.html'
    };
  });


})();
