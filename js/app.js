(function() {
  
  var app = angular.module('milkCalculator', [ 'navigation' ]);
  
  app.factory('UserService', function () {
    var user = {};   
    return {
      get: function () {
        return user;
      },
      set: function(newUser) {
        user = newUser;
        user.firstInitial = user.firstName.charAt(0) + '.';
      }
    };
  });

  app.controller('TeamController', [ '$http', '$scope', 'UserService', function ($http, $scope, UserService) {
    $http.get('data/team.json').success(function(data) {
      $scope.team = data.team;
    });
    $scope.selectUser = function (player) {
      UserService.set(player);
    };
  }]);  
  
  app.controller('RecommendController', [ '$http', '$scope', function ($http, $scope) {
    $http.get('data/dairy.json').success(function(data) {
      $scope.dairy = data;   
    });
  }]);
  
  app.controller('UserController', ['$scope', 'UserService', function ($scope, UserService) { 
    $scope.$watch(function () { return UserService.get(); }, function (newValue, oldValue) {
      if (newValue !== oldValue) $scope.newUser = newValue;
    });
  }]);
  
})();
